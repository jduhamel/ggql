// Copyright 2020 University Health Network. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func stubInput(t *ggql.Input) (err error) {
	var b strings.Builder

	b.WriteString(fmt.Sprintf("package %s\n\n", pkg))
	for _, f := range t.Fields() {
		if f.Type.Name() == timeStr {
			b.WriteString("import \"time\"\n\n")
			break
		}
	}
	desc := t.Description()
	if len(desc) == 0 {
		desc = dotdotdot
	}
	if strings.HasPrefix(desc, t.Name()+" ") {
		b.WriteString(fmt.Sprintf("// %s\n", strings.ReplaceAll(desc, "\n", "\n// ")))
	} else {
		b.WriteString(fmt.Sprintf("// %s %s\n", t.Name(), strings.ReplaceAll(desc, "\n", "\n// ")))
	}
	b.WriteString(fmt.Sprintf("type %s struct {\n", t.Name()))
	for _, f := range t.Fields() {
		public := publicName(f.N)
		desc = f.Desc
		if len(desc) == 0 {
			desc = dotdotdot
		}
		if strings.HasPrefix(desc, public+" ") {
			b.WriteString(fmt.Sprintf("\n\t// %s\n", strings.ReplaceAll(desc, "\n", "\n\t// ")))
		} else {
			b.WriteString(fmt.Sprintf("\n\t// %s %s\n", public, strings.ReplaceAll(desc, "\n", "\n\t// ")))
		}
		b.WriteString(fmt.Sprintf("\t%s %s\n", public, typeStr(f.Type)))
	}
	b.WriteString("}\n")
	path := filepath.Join(stubDir, strings.ToLower(t.Name())+".go")

	return ioutil.WriteFile(path, []byte(b.String()), 0666)
}
