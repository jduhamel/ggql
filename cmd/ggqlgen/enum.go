// Copyright 2020 University Health Network. All rights reserved.

package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func stubEnum(t *ggql.Enum) (err error) {
	var b strings.Builder

	b.WriteString(fmt.Sprintf("package %s\n\n", pkg))
	b.WriteString("const (\n")
	for _, ev := range t.Values() {
		name := t.Name() + string(ev.Value)
		desc := ev.Description
		if len(desc) == 0 {
			desc = dotdotdot
		}
		if strings.HasPrefix(desc, name+" ") {
			b.WriteString(fmt.Sprintf("\t// %s\n", strings.ReplaceAll(desc, "\n", "\n\t// ")))
		} else {
			b.WriteString(fmt.Sprintf("\t// %s %s\n", name, strings.ReplaceAll(desc, "\n", "\n\t// ")))
		}
		b.WriteString(fmt.Sprintf("\t%s = %q\n", name, string(ev.Value)))
	}
	b.WriteString(")\n")

	path := filepath.Join(stubDir, strings.ToLower(t.Name())+".go")

	return ioutil.WriteFile(path, []byte(b.String()), 0666)
}
