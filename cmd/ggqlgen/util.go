// Copyright 2020 University Health Network. All rights reserved.

package main

import (
	"strings"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func typeStr(t ggql.Type) string {
	// Handle the core scalars first.
	switch t.Name() {
	case "String", "ID":
		return "string"
	case "Int":
		return "int"
	case "Float":
		return "float32"
	case timeStr:
		return "time.Time"
	case "Boolean":
		return "bool"
	}
	switch tt := t.(type) {
	case *ggql.Enum:
		return "string"
	case *ggql.Input, *ggql.Object:
		return "*" + t.Name()
	case *ggql.List:
		return "[]" + typeStr(tt.Base)
	case *ggql.NonNull:
		return typeStr(tt.Base)
	}
	return "interface{}"
}

func publicName(s string) string {
	public := strings.Title(s)
	public = strings.ReplaceAll(public, "Id", "ID")
	public = strings.ReplaceAll(public, "id", "ID")
	return public
}
