// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestScalar(t *testing.T) {
	dir := ggql.Directive{
		Base: ggql.Base{
			N: "dirt",
		},
		On: []ggql.Location{ggql.LocScalar},
	}
	scalar := &ggql.Scalar{
		Base: ggql.Base{
			N:    "Scale",
			Desc: "A scalar example.",
			Dirs: []*ggql.DirectiveUse{{Directive: &dir}},
		},
	}
	checkEqual(t, false, scalar.Core(), "Scalar Core() mismatch")
	checkEqual(t, `"A scalar example."
scalar Scale @dirt
`, scalar.SDL(true), "Scalar SDL() mismatch")
	checkEqual(t, "Scale", scalar.String(), "Scalar String() mismatch")
	checkEqual(t, 10, scalar.Rank(), "Scalar Rank() mismatch")
	checkEqual(t, "A scalar example.", scalar.Description(), "Scalar Description() mismatch")
	checkEqual(t, 1, len(scalar.Directives()), "Scalar Directives() mismatch")

	w := &failWriter{max: 15}
	err := scalar.Write(w, false)
	checkNotNil(t, err, "return error on write error")
}
