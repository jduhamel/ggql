// Copyright 2020 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestFieldDefArgs(t *testing.T) {
	fd := ggql.FieldDef{}
	checkEqual(t, 0, len(fd.Args()), "FieldDef args should be empty")
}
