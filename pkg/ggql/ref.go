// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"io"
)

// Ref is used as a place holder in a two pass parse and resolve when parsing
// an SDL. It is public so it can be tested for 100% coverage.
type Ref struct {
	Base
}

// String representation of the instance.
func (t *Ref) String() string {
	return t.N
}

// SDL returns an SDL representation of the type.
func (t *Ref) SDL(desc ...bool) string {
	return ""
}

// Write the type as SDL.
func (t *Ref) Write(w io.Writer, desc bool) error {
	return nil
}

// Rank returns the rank of the type. Used in sorting the SDL output.
func (t *Ref) Rank() int {
	return rankHidden
}
