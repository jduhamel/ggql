// Copyright 2019 University Health Network. All rights reserved.

package ggql

import "fmt"

type inputFieldList struct {
	dict map[string]*InputField
	list []*InputField
}

func (il *inputFieldList) Nth(i int) (n interface{}) {
	if 0 <= i && i < len(il.list) {
		n = il.list[i]
	}
	return
}

func (il *inputFieldList) Len() int {
	return len(il.list)
}

func (il *inputFieldList) add(fds ...*InputField) error {
	if il.dict == nil {
		il.dict = map[string]*InputField{}
	}
	for _, fd := range fds {
		name := fd.Name()
		if il.dict[name] != nil {
			return fmt.Errorf("%w input field %s", ErrDuplicate, name)
		}
		il.dict[name] = fd
		il.list = append(il.list, fd)
	}
	return nil
}

func (il *inputFieldList) get(name string) (i *InputField) {
	if il.dict != nil {
		i = il.dict[name]
	}
	return
}
