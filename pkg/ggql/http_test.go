// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

// When parsing from a HTTP request the last character read will be provided
// along with an EOF. This test make sure the last character is not
// ignored. Tried io.Pipe() and that does not have the same behavior so
// resorted to HTTP client server.
func TestParseHTTP(t *testing.T) {
	ggql.Sort = true
	port := 11954
	var log strings.Builder
	root := setupTestSongs(t, &log)

	http.HandleFunc("/graphql", func(w http.ResponseWriter, req *http.Request) {
		defer func() { _ = req.Body.Close() }()

		result := root.ResolveReader(req.Body, "", nil)

		_ = ggql.WriteJSONValue(w, result, -1)
	})
	go func() { _ = http.ListenAndServe(fmt.Sprintf(":%d", port), nil) }()

	u := fmt.Sprintf("http://localhost:%d/graphql", port)
	res, err := http.Post(u, "application/graphql", strings.NewReader(`{artist(name: "Fazerdaze"){name}}`))
	checkNil(t, err, "POST failed. %s", err)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	checkEqual(t, `{"data":{"artist":{"name":"Fazerdaze"}}}`, string(body), "parsed vs given")
}
