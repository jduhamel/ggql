// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"fmt"
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestIDScalarCoerceIn(t *testing.T) {
	root := ggql.NewRoot(nil)
	tt := root.GetType("ID")
	scalar, _ := tt.(ggql.InCoercer)
	checkNotNil(t, scalar, "ID type should be a Coercer")

	id, err := scalar.CoerceIn("abc123")
	checkEqual(t, "abc123", id, "CoerceIn value mismatch")
	checkNil(t, err, "CoerceIn error. %s", err)

	id, err = scalar.CoerceIn(int32(123456))
	checkEqual(t, "123456", id, "CoerceIn value mismatch")
	checkNil(t, err, "CoerceIn error. %s", err)

	id, err = scalar.CoerceIn(int64(123456))
	checkEqual(t, "123456", id, "CoerceIn value mismatch")
	checkNil(t, err, "CoerceIn error. %s", err)

	_, err = scalar.CoerceIn(true)
	checkNotNil(t, err, "CoerceIn error")

	var r interface{}
	r, err = scalar.CoerceIn(nil)
	checkNil(t, r, "CoerceIn value mismatch")
	checkNil(t, err, "CoerceIn error. %s", err)
}

func TestIDScalarCoerceOut(t *testing.T) {
	root := ggql.NewRoot(nil)
	tt := root.GetType("ID")
	scalar, _ := tt.(ggql.OutCoercer)
	checkNotNil(t, scalar, "ID type should be a Coercer")

	for _, v := range []interface{}{
		int(3),
		int8(3),
		int16(3),
		int32(3),
		int64(3),
		uint(3),
		uint8(3),
		uint16(3),
		uint32(3),
		uint64(3),
		"rope",
	} {
		r, err := scalar.CoerceOut(v)
		checkEqual(t, fmt.Sprintf("%v", v), r, "CoerceOut value mismatch")
		checkNil(t, err, "CoerceOut error. %s", err)
	}
	r, err := scalar.CoerceOut(nil)
	checkNil(t, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	_, err = scalar.CoerceOut([]string{})
	checkNotNil(t, err, "CoerceOut error")
}
