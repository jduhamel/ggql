// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"bytes"
	"strings"
)

// Inline is a GraphQL execution request inline fragment.
type Inline struct {
	SelBase

	// Condition is the type that the fragment should be applied to.
	Condition Type
}

// String representation of the instance.
func (in *Inline) String() string {
	var b bytes.Buffer

	in.write(&b, 0)

	return b.String()
}

// Validate a type.
func (in *Inline) Validate(root *Root) (errs []error) {
	errs = append(errs, in.SelBase.Validate(root)...)
	for _, du := range in.Directives() {
		errs = append(errs, root.validateDirUse("...", Locate(in), du)...)
	}
	// Additional argument checks are performed during the resolve phase so no
	// need to attempt to validate argument type matching and coerce success.
	return
}

func (in *Inline) write(buf *bytes.Buffer, depth int) {
	_, _ = buf.WriteString(strings.Repeat("  ", depth))
	_, _ = buf.WriteString("...")
	if in.Condition != nil {
		_, _ = buf.WriteString(" on ")
		_, _ = buf.WriteString(in.Condition.Name())
	}
	_ = writeDirectiveUses(buf, in.Dirs)
	in.writeSels(buf, depth)
}
