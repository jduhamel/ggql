// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestField(t *testing.T) {
	dirt := ggql.Directive{
		Base: ggql.Base{N: "dirt"},
		On:   []ggql.Location{ggql.LocInlineFragment, ggql.LocField},
	}
	f := ggql.Field{
		Name: "user",
		Args: []*ggql.ArgValue{
			{Arg: "name", Value: "Gobo"},
			{Arg: "age", Value: 50},
		},
		SelBase: ggql.SelBase{Dirs: []*ggql.DirectiveUse{{Directive: &dirt}}},
	}
	checkEqual(t, `user(name: "Gobo", age: 50) @dirt`, f.String(), "Field.String() mismatch")

	checkNotNil(t, f.Directives(), "directives should not be nil")
	checkNotNil(t, f.SelectionSet(), "selections should not be nil")
}
