// Copyright 2019 University Health Network. All rights reserved.

package ggql

// Var is a GraphQL variable.
type Var string
