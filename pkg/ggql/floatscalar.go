// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"strconv"
)

type floatScalar struct {
	Scalar
}

func newFloatScalar() Type {
	return &floatScalar{
		Scalar{
			Base: Base{
				N:    "Float",
				core: true,
			},
		},
	}
}

// CoerceIn coerces an input value into the expected input type if possible
// otherwise an error is returned.
func (*floatScalar) CoerceIn(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
		// remains nil
	case float64:
		v = float32(tv)
	case float32:
		// ok as is
	case int32:
		v = float32(tv)
	case int64:
		v = float32(tv)
	default:
		v = nil
		err = newCoerceErr(tv, "Float")
	}
	return v, err
}

// CoerceOut coerces a result value into a type for the scalar.
func (t *floatScalar) CoerceOut(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
		// remains nil
	case float32:
		// ok as is
	case float64:
		v = float32(tv)
	case int:
		v = float32(tv)
	case int8:
		v = float32(tv)
	case int16:
		v = float32(tv)
	case int32:
		v = float32(tv)
	case int64:
		v = float32(tv)
	case uint:
		v = float32(tv)
	case uint8:
		v = float32(tv)
	case uint16:
		v = float32(tv)
	case uint32:
		v = float32(tv)
	case uint64:
		v = float32(tv)
	case string:
		var f float64
		if f, err = strconv.ParseFloat(tv, 64); err == nil {
			v = float32(f)
		}
	default:
		v = nil
		err = newCoerceErr(tv, "Float")
	}
	return v, err
}
