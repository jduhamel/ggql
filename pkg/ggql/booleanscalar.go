// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"strconv"
)

type booleanScalar struct {
	Scalar
}

func newBooleanScalar() Type {
	return &booleanScalar{
		Scalar{
			Base: Base{
				N:    "Boolean",
				core: true,
			},
		},
	}
}

// CoerceIn coerces an input value into the expected input type if possible
// otherwise an error is returned.
func (*booleanScalar) CoerceIn(v interface{}) (interface{}, error) {
	if v == nil {
		return nil, nil
	}
	if b, ok := v.(bool); ok {
		return b, nil
	}
	return nil, newCoerceErr(v, "Boolean")
}

// CoerceOut coerces a result value into a type for the scalar.
func (t *booleanScalar) CoerceOut(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
		// remains nil
	case bool:
		// ok as is
	case float32:
		v = tv != 0.0
	case int32:
		v = tv != 0
	case string:
		var b bool
		if b, err = strconv.ParseBool(tv); err == nil {
			v = b
		}
	default:
		err = newCoerceErr(tv, "Boolean")
		v = nil
	}
	return v, err
}
