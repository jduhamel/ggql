// Copyright 2019 University Health Network. All rights reserved.

package ggql

// ListResolver is the interface for resolving lists.
type ListResolver interface {

	// Len of the list.
	Len() int

	// Nth element in the list.
	Nth(i int) interface{}
}
