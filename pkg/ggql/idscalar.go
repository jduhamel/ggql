// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"strconv"
)

type idScalar struct {
	Scalar
}

func newIDScalar() Type {
	return &idScalar{
		Scalar{
			Base: Base{
				N:    "ID",
				core: true,
			},
		},
	}
}

// CoerceIn coerces an input value into the expected input type if possible
// otherwise an error is returned.
func (*idScalar) CoerceIn(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
		// remains nil
	case int32:
		v = strconv.Itoa(int(tv))
	case int64:
		v = strconv.FormatInt(tv, 10)
	case int:
		v = strconv.Itoa(tv)
	case string:
		// ok as is
	default:
		err = newCoerceErr(tv, "ID")
		v = nil
	}
	return v, err
}

// CoerceOut coerces a result value into a type for the scalar.
func (t *idScalar) CoerceOut(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
	// remains nil
	case int:
		v = strconv.Itoa(tv)
	case int8:
		v = strconv.Itoa(int(tv))
	case int16:
		v = strconv.Itoa(int(tv))
	case int32:
		v = strconv.Itoa(int(tv))
	case int64:
		v = strconv.FormatInt(tv, 10)
	case uint:
		v = strconv.Itoa(int(tv))
	case uint8:
		v = strconv.Itoa(int(tv))
	case uint16:
		v = strconv.Itoa(int(tv))
	case uint32:
		v = strconv.Itoa(int(tv))
	case uint64:
		v = strconv.FormatInt(int64(tv), 10)
	case string:
		// ok as is
	default:
		err = newCoerceErr(tv, "ID")
		v = nil
	}
	return v, err
}
