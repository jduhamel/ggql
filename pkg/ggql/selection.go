// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"bytes"
	"fmt"
)

// Selection represents a member of a GraphQL selection set.
type Selection interface {
	fmt.Stringer

	// Directives returns the directive associated with the type.
	Directives() []*DirectiveUse

	// SelectionSet or child Selections.
	SelectionSet() []Selection

	// Validate a type.
	Validate(root *Root) []error

	// Line the selection was defined on in the request.
	Line() int

	// Column of the start of the selection in the request.
	Column() int

	write(buf *bytes.Buffer, depth int)
}
