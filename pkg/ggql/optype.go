// Copyright 2019 University Health Network. All rights reserved.

package ggql

// OpType is the operation type for a execution request operation.
type OpType string

const (
	// OpQuery is a query operation.
	OpQuery = OpType("query")

	// OpMutation is a mutation operation.
	OpMutation = OpType("mutation")

	// OpSubscription is a subscription operation.
	OpSubscription = OpType("subscription")
)
