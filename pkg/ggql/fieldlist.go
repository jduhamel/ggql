// Copyright 2019 University Health Network. All rights reserved.

package ggql

import "fmt"

type fieldList struct {
	dict map[string]*FieldDef
	list []*FieldDef
}

func (fl *fieldList) Nth(i int) (n interface{}) {
	if 0 <= i && i < len(fl.list) {
		n = fl.list[i]
	}
	return
}

func (fl *fieldList) Len() int {
	return len(fl.list)
}

func (fl *fieldList) add(fds ...*FieldDef) error {
	if fl.dict == nil {
		fl.dict = map[string]*FieldDef{}
	}
	for _, fd := range fds {
		name := fd.Name()
		if fl.dict[name] != nil {
			return fmt.Errorf("%w field %s", ErrDuplicate, name)
		}
		fl.dict[name] = fd
		fl.list = append(fl.list, fd)
	}
	return nil
}

func (fl *fieldList) get(name string) (f *FieldDef) {
	if fl.dict != nil {
		f = fl.dict[name]
	}
	return
}
