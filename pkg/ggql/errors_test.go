// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"fmt"
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestErrors(t *testing.T) {
	err := ggql.Errors([]error{
		fmt.Errorf("first error"),
		fmt.Errorf("second error"),
	})
	checkEqual(t, `Errors{
  first error
  second error
}
`, err.Error(), "expect multiple lines")
}
