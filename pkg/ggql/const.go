// Copyright 2019 University Health Network. All rights reserved.

package ggql

const (
	trueStr  = "true"
	falseStr = "false"
	nullStr  = "null"

	argsStr              = "args"
	booleanStr           = "Boolean"
	defaultValueStr      = "defaultValue"
	deprecatedStr        = "deprecated"
	deprecationReasonStr = "deprecationReason"
	descriptionStr       = "description"
	directiveStr         = "directive"
	enumStr              = "enum"
	enumValuesStr        = "enumValues"
	extendStr            = "extend"
	fieldsStr            = "fields"
	includeDeprecatedStr = "includeDeprecated"
	inputFieldsStr       = "inputFields"
	inputStr             = "input"
	interfaceStr         = "interface"
	interfacesStr        = "interfaces"
	isDeprecatedStr      = "isDeprecated"
	kindStr              = "kind"
	locationsStr         = "locations"
	nameStr              = "name"
	ofTypeStr            = "ofType"
	possibleTypesStr     = "possibleTypes"
	reasonStr            = "reason"
	scalarStr            = "scalar"
	schemaStr            = "schema"
	stringStr            = "String"
	typeStr              = "type"
	unionStr             = "union"
)
