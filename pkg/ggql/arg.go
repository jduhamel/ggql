// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"io"
)

// Arg is a GraphQL Arg or InputValue.
type Arg struct {
	Base

	// Type for the arg value.
	Type Type

	// Default value for the argument.
	Default interface{}
}

// Write the type as SDL.
func (a *Arg) Write(w io.Writer, desc bool) (err error) {
	err = writeDesc(w, a.Desc, 2, desc)
	if err == nil {
		_, err = w.Write([]byte(a.N))
	}
	if err == nil {
		_, err = w.Write([]byte{':', ' '})
	}
	if err == nil {
		_, err = w.Write([]byte(a.Type.Name()))
	}
	if err == nil && a.Default != nil {
		_, err = w.Write([]byte{' ', '=', ' '})
		if err == nil {
			_, err = w.Write([]byte(valueString(a.Default)))
		}
	}
	if err == nil {
		err = writeDirectiveUses(w, a.Dirs)
	}
	return
}

func writeArgs(w io.Writer, args *argList, desc bool) (err error) {
	if 0 < args.Len() {
		if _, err = w.Write([]byte{'('}); err == nil {
			for i, a := range args.list {
				if 0 < i {
					_, err = w.Write([]byte{','})
					if err == nil && !desc || len(a.Desc) == 0 {
						_, err = w.Write([]byte{' '})
					}
				}
				if err == nil {
					err = a.Write(w, desc)
				}
				if err != nil {
					break
				}
			}
			if err == nil {
				_, err = w.Write([]byte{')'})
			}

		}
	}
	return
}

// Resolve returns one of the following:
//   name: String!
//   description: String
//   type: __Type!
//   defaultValue: String
func (a *Arg) Resolve(field *Field, args map[string]interface{}) (result interface{}, err error) {
	switch field.Name {
	case nameStr:
		result = a.N
	case descriptionStr:
		result = a.Desc
	case typeStr:
		result = a.Type
	case defaultValueStr:
		result = a.Default
	}
	return
}
