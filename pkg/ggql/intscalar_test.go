// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestIntScalarCoerceIn(t *testing.T) {
	root := ggql.NewRoot(nil)
	tt := root.GetType("Int")
	scalar, _ := tt.(ggql.InCoercer)
	checkNotNil(t, scalar, "Int type should be a Coercer")

	for _, v := range []interface{}{
		int(3),
		int8(3),
		int16(3),
		int32(3),
		int64(3),
		uint(3),
		uint8(3),
		uint16(3),
		uint32(3),
		uint64(3),
	} {
		r, err := scalar.CoerceIn(v)
		checkEqual(t, 3, r, "Int.CoerceIn(%v) value mismatch", v)
		checkNil(t, err, "Int.CoerceIn(%v) error. %s", v, err)
	}
	b, err := scalar.CoerceIn(int32(3))
	checkEqual(t, 3, b, "Int.CoerceIn(3) value mismatch")
	checkNil(t, err, "Int.CoerceIn(3) error. %s", err)

	_, err = scalar.CoerceIn(true)
	checkNotNil(t, err, "Int.CoerceIn(true) error")

	v, err := scalar.CoerceIn(nil)
	checkNil(t, err, "Int.CoerceIn(nil) error. %s", err)
	checkNil(t, v, "Int.CoerceIn(nil) should return nil")
}

func TestIntScalarCoerceOut(t *testing.T) {
	root := ggql.NewRoot(nil)
	tt := root.GetType("Int")
	scalar, _ := tt.(ggql.OutCoercer)
	checkNotNil(t, scalar, "Int type should be a OutCoercer")

	for _, v := range []interface{}{
		float32(3.1),
		float64(3.3),
		int(3),
		int8(3),
		int16(3),
		int32(3),
		int64(3),
		uint(3),
		uint8(3),
		uint16(3),
		uint32(3),
		uint64(3),
		"3",
	} {
		r, err := scalar.CoerceOut(v)
		checkEqual(t, 3, r, "Int.CoerceOut(%v) value mismatch", v)
		checkNil(t, err, "Int.CoerceOut(%v) error. %s", v, err)
	}
	r, err := scalar.CoerceOut(nil)
	checkNil(t, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	_, err = scalar.CoerceOut("1.23")
	checkNotNil(t, err, `Int.CoerceOut("1.23") error`)

	_, err = scalar.CoerceOut(true)
	checkNotNil(t, err, "Int.CoerceOut(true) error")
}
