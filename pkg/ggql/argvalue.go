// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"io"
)

// Sort output values in Object type (maps).
var Sort = false

// ArgValue is a GraphQL Arg value.
type ArgValue struct {
	// Arg name associated with the value.
	Arg string

	// Value of the object.
	Value interface{}

	line int
	col  int
}

// Write the type as SDL.
func (av *ArgValue) Write(w io.Writer) (err error) {
	if _, err = w.Write([]byte(av.Arg + ": ")); err == nil {
		_, err = w.Write([]byte(valueString(av.Value)))
	}
	return
}
