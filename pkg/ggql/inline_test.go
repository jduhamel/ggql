// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestInline(t *testing.T) {
	in := ggql.Inline{
		Condition: &ggql.Ref{Base: ggql.Base{N: "User"}},
		SelBase: ggql.SelBase{
			Sels: []ggql.Selection{
				&ggql.Field{Name: "height"},
			},
		},
	}
	checkEqual(t, `... on User {
  height
}`, in.String(), "Inline.String() mismatch")
}
