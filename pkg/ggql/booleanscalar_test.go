// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestBooleanScalarCoerceIn(t *testing.T) {
	root := ggql.NewRoot(nil)
	tt := root.GetType("Boolean")
	scalar, _ := tt.(ggql.InCoercer)
	checkNotNil(t, scalar, "Boolean type should be a Coercer")

	b, err := scalar.CoerceIn(true)
	checkEqual(t, true, b, "CoerceIn value mismatch")
	checkNil(t, err, "CoerceIn error. %s", err)

	_, err = scalar.CoerceIn(3)
	checkNotNil(t, err, "CoerceIn error")

	var r interface{}
	r, err = scalar.CoerceIn(nil)
	checkNil(t, r, "CoerceIn value mismatch")
	checkNil(t, err, "CoerceIn error. %s", err)
}

func TestBooleanScalarCoerceOut(t *testing.T) {
	root := ggql.NewRoot(nil)
	tt := root.GetType("Boolean")
	scalar, _ := tt.(ggql.OutCoercer)
	checkNotNil(t, scalar, "Boolean type should be a Coercer")

	r, err := scalar.CoerceOut(float32(3.7))
	checkEqual(t, true, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	r, err = scalar.CoerceOut(float32(0.0))
	checkEqual(t, false, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	r, err = scalar.CoerceOut(int32(-3))
	checkEqual(t, true, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	r, err = scalar.CoerceOut(int32(0))
	checkEqual(t, false, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	r, err = scalar.CoerceOut("true")
	checkEqual(t, true, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	r, err = scalar.CoerceOut("false")
	checkEqual(t, false, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)

	_, err = scalar.CoerceOut("yes")
	checkNotNil(t, err, "CoerceOut error")

	_, err = scalar.CoerceOut([]string{})
	checkNotNil(t, err, "CoerceOut error")

	r, err = scalar.CoerceOut(nil)
	checkNil(t, r, "CoerceOut value mismatch")
	checkNil(t, err, "CoerceOut error. %s", err)
}
