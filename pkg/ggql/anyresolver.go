// Copyright 2019 University Health Network. All rights reserved.

package ggql

// AnyResolver is the interface for resolving fields on any data. It is
// generally used for unstructured data.
type AnyResolver interface {

	// Resolve a field on an object. The field argument is the name of the
	// field to resolve. The args parameter includes the values associated
	// with the arguments provided by the caller. The function should return
	// the field's object or an error. A return of nil is also possible.
	Resolve(obj interface{}, field *Field, args map[string]interface{}) (interface{}, error)

	// Len of the list.
	Len(list interface{}) int

	// Nth element in the list. If not a list or out of bounds nil should be
	// returned along with an error.
	Nth(list interface{}, i int) (interface{}, error)
}
