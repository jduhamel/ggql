// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestRef(t *testing.T) {
	ref := &ggql.Ref{Base: ggql.Base{N: "Dummy"}}
	// Never add a ref directly. The type is only public so that code coverage
	// on unit test can be closer to 100%.

	checkEqual(t, "", ref.SDL(false), "Ref SDL() mismatch")
	checkEqual(t, "Dummy", ref.String(), "Ref String() mismatch")
	checkEqual(t, 0, ref.Rank(), "Ref Rank() mismatch")

	checkNil(t, ref.Write(nil, false), "check Write returns nil")
}
