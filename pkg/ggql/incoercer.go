// Copyright 2019 University Health Network. All rights reserved.

package ggql

// InCoercer interface for scalars defines the function that is used to coerce
// an input value into the scalar type.
type InCoercer interface {

	// CoerceIn coerces an input value into the expected input type if possible
	// otherwise an error is returned.
	CoerceIn(v interface{}) (interface{}, error)
}
