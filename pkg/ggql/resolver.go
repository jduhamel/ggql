// Copyright 2019 University Health Network. All rights reserved.

package ggql

// Resolver is the interface for resolving fields on an object.
type Resolver interface {

	// Resolve a field on an object. The field argument is the name of the
	// field to resolve. The args parameter includes the values associated
	// with the arguments provided by the caller. The function should return
	// the field's object or an error. A return of nil is also possible.
	Resolve(field *Field, args map[string]interface{}) (interface{}, error)
}
