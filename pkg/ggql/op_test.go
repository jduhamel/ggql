// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestOp(t *testing.T) {
	hello := ggql.Field{
		Name: "hello",
		Args: []*ggql.ArgValue{{Arg: "name", Value: ggql.Var("name")}},
	}
	op := ggql.Op{
		SelBase: ggql.SelBase{Sels: []ggql.Selection{&hello}},
		Type:    ggql.OpQuery,
	}
	checkEqual(t, `query {
  hello(name: $name)
}
`, op.String(), "incorrect output for blank op")

	op.Name = "greeting"
	checkEqual(t, `query greeting {
  hello(name: $name)
}
`, op.String(), "incorrect output for named op")

	op.Variables = []*ggql.VarDef{{Name: "name", Type: &ggql.Ref{Base: ggql.Base{N: "String"}}, Default: "sailor"}}
	checkEqual(t, `query greeting($name: String = "sailor") {
  hello(name: $name)
}
`, op.String(), "incorrect output for op with variables")

	hello.Alias = "hi"
	checkEqual(t, `query greeting($name: String = "sailor") {
  hi: hello(name: $name)
}
`, op.String(), "incorrect output for op with variables")

	dirt := ggql.Directive{
		Base: ggql.Base{
			N: "dirt",
		},
		On: []ggql.Location{ggql.LocArgumentDefinition, ggql.LocVariableDefinition},
	}
	op.Dirs = []*ggql.DirectiveUse{{Directive: &dirt}}

	checkEqual(t, `query greeting($name: String = "sailor") @dirt {
  hi: hello(name: $name)
}
`, op.String(), "incorrect output for op with variables and directives")

	op.Variables[0].Dirs = []*ggql.DirectiveUse{{Directive: &dirt}}
	checkEqual(t, `query greeting($name: String = "sailor" @dirt) @dirt {
  hi: hello(name: $name)
}
`, op.String(), "incorrect output for op with variables and more directives")

	op.Variables = append(op.Variables, &ggql.VarDef{Name: "age", Type: &ggql.Ref{Base: ggql.Base{N: "Int"}}})
	checkEqual(t, `query greeting($name: String = "sailor" @dirt, $age: Int) @dirt {
  hi: hello(name: $name)
}
`, op.String(), "incorrect output for op with variables and directives")
}
