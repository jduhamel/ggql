// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestFragRef(t *testing.T) {
	f := ggql.FragRef{
		Fragment: &ggql.Fragment{
			Name: "fraggle",
			Inline: ggql.Inline{
				SelBase: ggql.SelBase{
					Sels: []ggql.Selection{},
				},
			},
		},
		Dirs: []*ggql.DirectiveUse{},
	}
	checkEqual(t, "...fraggle", f.String(), "FragRef.String() mismatch")
	checkNotNil(t, f.Directives(), "FragRef.Directives() returned nil")
	checkNotNil(t, f.SelectionSet(), "FragRef.SelectionSet() returned nil")
}
