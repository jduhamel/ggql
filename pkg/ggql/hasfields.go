// Copyright 2019 University Health Network. All rights reserved.

package ggql

// HasFields is the interface for finding a field definition on an instance.
type HasFields interface {

	// GetField returns the field matching the name or nil if not found.
	GetField(name string) *FieldDef
}
