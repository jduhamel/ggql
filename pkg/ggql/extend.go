// Copyright 2019 University Health Network. All rights reserved.

package ggql

// Extend is a GraphQL Object.
type Extend struct {

	// Adds are the additions to a type.
	Adds Type
}
