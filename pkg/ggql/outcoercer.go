// Copyright 2019 University Health Network. All rights reserved.

package ggql

// OutCoercer interface for scalars defines the function that is used to coerce
// a scalar value into an output type that can be encoded as JSON.
type OutCoercer interface {

	// CoerceOut coerces a result value into a value suitable for output.
	CoerceOut(v interface{}) (interface{}, error)
}
