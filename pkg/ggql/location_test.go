// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestLocate(t *testing.T) {
	for loc, v := range map[ggql.Location]interface{}{
		ggql.LocObject:               &ggql.Object{},
		ggql.LocSchema:               &ggql.Schema{},
		ggql.LocFieldDefinition:      &ggql.FieldDef{},
		ggql.LocArgumentDefinition:   &ggql.ArgValue{},
		ggql.LocInputFieldDefinition: &ggql.Arg{},
		ggql.LocInterface:            &ggql.Interface{},
		ggql.LocUnion:                &ggql.Union{},
		ggql.LocEnum:                 &ggql.Enum{},
		ggql.LocEnumValue:            &ggql.EnumValue{},
		ggql.LocInputObject:          &ggql.Input{},
		ggql.LocScalar:               &ggql.Scalar{},
		"":                           "not locatable",
	} {
		checkEqual(t, string(loc), string(ggql.Locate(v)),
			"locate %T should return %s not '%s'", v, loc, ggql.Locate(v))
	}
}

func TestIsInputType(t *testing.T) {
	checkEqual(t, true, ggql.IsInputType(&ggql.Input{}), "Input should be an input type")
	checkEqual(t, true, ggql.IsInputType(&ggql.List{Base: &ggql.Input{}}), "[Input] should be an input type")
	checkEqual(t, true, ggql.IsInputType(&ggql.NonNull{Base: &ggql.Input{}}), "Input! should be an input type")
	checkEqual(t, true, ggql.IsInputType(&ggql.NonNull{Base: &ggql.List{Base: &ggql.NonNull{Base: &ggql.Input{}}}}), "[Input!]! should be an input type")
	checkEqual(t, false, ggql.IsInputType(&ggql.Object{}), "Object should not be an input type")
}

func TestIsoutputType(t *testing.T) {
	checkEqual(t, true, ggql.IsOutputType(&ggql.Object{}), "Output should be an output type")
	checkEqual(t, true, ggql.IsOutputType(&ggql.List{Base: &ggql.Object{}}), "[Output] should be an output type")
	checkEqual(t, true, ggql.IsOutputType(&ggql.NonNull{Base: &ggql.Object{}}), "Output! should be an output type")
	checkEqual(t, true, ggql.IsOutputType(&ggql.NonNull{Base: &ggql.List{Base: &ggql.NonNull{Base: &ggql.Object{}}}}), "[Output!]! should be an output type")
	checkEqual(t, false, ggql.IsOutputType(&ggql.Input{}), "Input should not be an output type")
}
