// Copyright 2019 University Health Network. All rights reserved.

package ggql

// Nester is used to create new context for fields. It allows a tree of user
// data to be built up for fields in an executable.
type Nester interface {

	// Nest should create a context. Usually it will be a new Nester but that
	// is not a requirement. The field provided is the current field the
	// context is being created for.
	Nest(field *Field) interface{}
}
