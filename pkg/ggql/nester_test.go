// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

type nest struct {
	buf *strings.Builder
}

func (n *nest) Nest(f *ggql.Field) interface{} {
	n.buf.WriteString(fmt.Sprintf("nest.Nest(%s)\n", f.Name))
	return &nest{buf: n.buf}
}

func TestNester(t *testing.T) {
	root := setupTestSongs(t, nil)

	exe, err := root.ParseExecutableString(`{artists{...Frag}}
fragment Frag on Artist {name,songs{...{name}}} 
`)
	checkNil(t, err, "ParseExecutableString should not fail. %s", err)

	var b strings.Builder
	n := nest{buf: &b}

	exe.SetContextRecursive(&n)

	expect := `nest.Nest(artists)
nest.Nest(name)
nest.Nest(songs)
nest.Nest(name)
`
	checkEqual(t, expect, b.String(), "nest output mismatch")
}
