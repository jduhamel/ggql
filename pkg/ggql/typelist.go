// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"sort"
	"strings"
)

type typeList struct {
	dict map[string]Type
	list []Type
}

func newTypeList() *typeList {
	return &typeList{dict: map[string]Type{}}
}

func (tl *typeList) Nth(i int) (n interface{}) {
	if 0 <= i && i < len(tl.list) {
		n = tl.list[i]
	}
	return
}

func (tl *typeList) Len() int {
	return len(tl.list)
}

func (tl *typeList) add(ts ...Type) {
	for _, t := range ts {
		tl.dict[t.Name()] = t
		tl.list = append(tl.list, t)
	}
	sort.Slice(tl.list, func(i, j int) bool {
		ti := tl.list[i]
		tj := tl.list[j]
		if ti.Rank() == tj.Rank() {
			return 0 > strings.Compare(ti.Name(), tj.Name())
		}
		return ti.Rank() < (tj.Rank())
	})
}

func (tl *typeList) get(name string) (t Type) {
	if tl.dict != nil {
		t = tl.dict[name]
	}
	return
}

func (tl *typeList) dup() *typeList {
	d := typeList{
		list: make([]Type, len(tl.list)),
		dict: map[string]Type{},
	}
	copy(d.list, tl.list)
	if tl.dict != nil {
		for name, t := range tl.dict {
			d.dict[name] = t
		}
	}
	return &d
}
