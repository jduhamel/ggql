// Copyright 2019 University Health Network. All rights reserved.

package ggql

import (
	"strconv"
)

type intScalar struct {
	Scalar
}

func newIntScalar() Type {
	return &intScalar{
		Scalar{
			Base: Base{
				N:    "Int",
				core: true,
			},
		},
	}
}

// CoerceIn coerces an input value into the expected input type if possible
// otherwise an error is returned.
func (*intScalar) CoerceIn(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
		// remains nil
	case int:
		v = int32(tv)
	case int8:
		v = int32(tv)
	case int16:
		v = int32(tv)
	case int32:
		// ok as is
	case int64:
		v = int32(tv)
	case uint:
		v = int32(tv)
	case uint8:
		v = int32(tv)
	case uint16:
		v = int32(tv)
	case uint32:
		v = int32(tv)
	case uint64:
		v = int32(tv)
	default:
		err = newCoerceErr(v, "Int")
		v = nil
	}
	return v, err
}

// CoerceOut coerces a result value into a type for the scalar.
func (t *intScalar) CoerceOut(v interface{}) (interface{}, error) {
	var err error
	switch tv := v.(type) {
	case nil:
		// remains nil
	case float32:
		v = int32(tv)
	case float64:
		v = int32(tv)
	case int:
		v = int32(tv)
	case int8:
		v = int32(tv)
	case int16:
		v = int32(tv)
	case int32:
		// ok as is
	case int64:
		v = int32(tv)
	case uint:
		v = int32(tv)
	case uint8:
		v = int32(tv)
	case uint16:
		v = int32(tv)
	case uint32:
		v = int32(tv)
	case uint64:
		v = int32(tv)
	case string:
		var i int64
		if i, err = strconv.ParseInt(tv, 10, 64); err == nil {
			v = int32(i)
		}
	default:
		err = newCoerceErr(tv, "Int")
		v = nil
	}
	return v, err
}
