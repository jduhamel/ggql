// Copyright 2019 University Health Network. All rights reserved.

package ggql

import "fmt"

type enumValueList struct {
	dict map[string]*EnumValue
	list []*EnumValue
}

func (el *enumValueList) Nth(i int) (n interface{}) {
	if 0 <= i && i < len(el.list) {
		n = el.list[i]
	}
	return
}

func (el *enumValueList) Len() int {
	return len(el.list)
}

func (el *enumValueList) add(evs ...*EnumValue) error {
	if el.dict == nil {
		el.dict = map[string]*EnumValue{}
	}
	for _, ev := range evs {
		name := string(ev.Value)
		if el.dict[name] != nil {
			return fmt.Errorf("%w enum value %s", ErrDuplicate, name)
		}
		el.dict[name] = ev
		el.dict[string(ev.Value)] = ev
		el.list = append(el.list, ev)
	}
	return nil
}
