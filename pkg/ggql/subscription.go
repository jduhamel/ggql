// Copyright 2019 University Health Network. All rights reserved.

package ggql

// Subscription encapsulates the information about a subscription.
type Subscription struct {
	sub   Subscriber
	field *Field
	args  map[string]interface{}
}

// NewSubscription creates a new subscription. It should be called in a
// Resolver.Resolve() call.
func NewSubscription(sub Subscriber, field *Field, args map[string]interface{}) *Subscription {
	return &Subscription{
		sub:   sub,
		field: field,
		args:  args,
	}
}

func (sub *Subscription) prep(root *Root) {
	sub.field.ConType = root.getFieldType(sub.field.ConType, sub.field.Name)
}
