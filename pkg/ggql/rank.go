// Copyright 2019 University Health Network. All rights reserved.

package ggql

const (
	rankHidden = iota
	rankSchema
	rankQuery
	rankMutation
	rankSubscription
	rankObject
	rankInput
	rankUnion
	rankInterface
	rankEnum
	rankScalar
	rankDirective
)
