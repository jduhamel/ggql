// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestUnion(t *testing.T) {
	root := ggql.NewRoot(nil)

	dirt := ggql.Directive{
		Base: ggql.Base{
			N: "dirt",
		},
		On: []ggql.Location{ggql.LocUnion},
	}
	a := &ggql.Object{Base: ggql.Base{N: "Aa"}}
	a.AddField(&ggql.FieldDef{Base: ggql.Base{N: "a"}, Type: root.GetType("String")})

	b := &ggql.Object{Base: ggql.Base{N: "Bb"}}
	b.AddField(&ggql.FieldDef{Base: ggql.Base{N: "b"}, Type: root.GetType("String")})
	u := &ggql.Union{
		Base: ggql.Base{
			N:    "Uu",
			Desc: "Union of A and B.",
			Dirs: []*ggql.DirectiveUse{{Directive: &dirt}},
		},
		Members: []ggql.Type{a, b},
	}
	err := root.AddTypes(u, a, b, &dirt)
	checkNil(t, err, "root.AddTypes failed. %s", err)
	actual := root.SDL(false, true)
	expectStr := `union Uu @dirt = Aa | Bb
`
	expectRoot := `
type Aa {
  a: String
}

type Bb {
  b: String
}

"Union of A and B."
` + expectStr + timeScalarSDL + `
directive @dirt on UNION
`

	checkEqual(t, expectRoot, actual, "Union SDL() mismatch")
	checkEqual(t, expectStr, u.String(), "Union String() mismatch")
	checkEqual(t, 7, u.Rank(), "Union Rank() mismatch")
	checkEqual(t, "Union of A and B.", u.Description(), "Union Description() mismatch")
	checkEqual(t, 1, len(u.Directives()), "Union Directives() mismatch")

	err = u.Extend(&ggql.Union{Base: ggql.Base{N: "Uu"}, Members: []ggql.Type{&ggql.Object{Base: ggql.Base{N: "Cc"}}}})
	checkNil(t, err, "union.Extend failed. %s", err)

	err = u.Extend(&ggql.Union{Base: ggql.Base{N: "Uu"}, Members: []ggql.Type{&ggql.Object{Base: ggql.Base{N: "Cc"}}}})
	checkNotNil(t, err, "duplicate member for union.Extend should have failed.")

	w := &failWriter{max: 8}
	err = u.Write(w, false)
	checkNotNil(t, err, "return error on write error")

	w = &failWriter{max: 10}
	err = u.Write(w, false)
	checkNotNil(t, err, "return error on write error")

	w = &failWriter{max: 18}
	err = u.Write(w, false)
	checkNotNil(t, err, "return error on write error")

}
