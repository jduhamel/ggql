// Copyright 2019 University Health Network. All rights reserved.

package ggql

import "fmt"

type argList struct {
	dict map[string]*Arg
	list []*Arg
}

func (al *argList) Nth(i int) (n interface{}) {
	if 0 <= i && i < len(al.list) {
		n = al.list[i]
	}
	return
}

func (al *argList) Len() int {
	return len(al.list)
}

func (al *argList) add(fds ...*Arg) error {
	if al.dict == nil {
		al.dict = map[string]*Arg{}
	}
	for _, fd := range fds {
		name := fd.Name()
		if al.dict[name] != nil {
			return fmt.Errorf("%w argument %s", ErrDuplicate, name)
		}
		al.dict[name] = fd
		al.list = append(al.list, fd)
	}
	return nil
}

func (al *argList) get(name string) (a *Arg) {
	if al.dict != nil {
		a = al.dict[name]
	}
	return
}
