// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestDirective(t *testing.T) {
	root := ggql.NewRoot(nil)

	typeArg := ggql.Arg{
		Base: ggql.Base{
			N: "type",
		},
		Type: &ggql.NonNull{
			Base: root.GetType("String"),
		},
	}
	xDir := ggql.Directive{
		Base: ggql.Base{
			N:    "example",
			Desc: "Associates a GraphQLtype with a golang type.",
		},
		On: []ggql.Location{ggql.LocObject, ggql.LocSchema},
	}
	xDir.AddArg(&typeArg)

	err := root.AddTypes(&xDir)
	checkNil(t, err, "root.AddTypes failed. %s", err)
	actual := root.SDL(false, true)
	expectStr := "directive @example(type: String!) on OBJECT | SCHEMA"
	expect := timeScalarSDL + `
"Associates a GraphQLtype with a golang type."
` + expectStr + "\n"

	checkEqual(t, expect, actual, "Directive SDL() mismatch")
	checkEqual(t, expectStr, xDir.String(), "Directive String() mismatch")
	checkEqual(t, 11, xDir.Rank(), "Directive Rank() mismatch")
	checkEqual(t, "Associates a GraphQLtype with a golang type.", xDir.Description(), "Directive Description() mismatch")

	w := &failWriter{max: len(expectStr) - 5}
	err = xDir.Write(w, false)
	checkNotNil(t, err, "return error on write error")

	checkNotNil(t, xDir.Extend(nil), "return error on extend attempt")
}

func TestDirectiveBadRef(t *testing.T) {
	root := ggql.NewRoot(nil)

	dir := ggql.Directive{
		Base: ggql.Base{N: "example"},
		On:   []ggql.Location{ggql.LocObject, ggql.LocSchema},
	}
	dir.AddArg(&ggql.Arg{Base: ggql.Base{N: "bad"}, Type: &ggql.Ref{Base: ggql.Base{N: "Bad"}}})

	err := root.AddTypes(&dir)
	checkNotNil(t, err, "root.AddTypes with bad ref should fail.")

	dir = ggql.Directive{
		Base: ggql.Base{N: "example"},
		On:   []ggql.Location{ggql.LocObject, ggql.LocSchema},
	}
	dir.AddArg(&ggql.Arg{
		Base: ggql.Base{N: "bad", Dirs: []*ggql.DirectiveUse{{Directive: &ggql.Ref{Base: ggql.Base{N: "Bad"}}}}},
		Type: root.GetType("String"),
	})

	err = root.AddTypes(&dir)
	checkNotNil(t, err, "root.AddTypes with bad arg directive ref should fail.")
}
