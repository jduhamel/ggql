// Copyright 2019 University Health Network. All rights reserved.

package ggql

// uuSchema is a GraphQL schema.
type uuSchema struct {
	root *Root
	Object
}

// Rank of the type.
func (t *uuSchema) Rank() int {
	return rankSchema
}
