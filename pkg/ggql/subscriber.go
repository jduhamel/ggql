// Copyright 2019 University Health Network. All rights reserved.

package ggql

// Subscriber is the interface for subscription implementations.
type Subscriber interface {

	// Send an event on channel associated with the subscription.
	Send(value interface{}) error

	// Match an eventID to the subscriber. Behavior is up to the
	// subscriber. An exact match could be used or a system that uses
	// wildcards can be used. A return of true indicates a match.
	Match(eventID string) bool

	// Unsubscribe should clean up any resources associated with a
	// subscription.
	Unsubscribe()
}
