// Copyright 2019 University Health Network. All rights reserved.

package ggql_test

import (
	"testing"

	"gitlab.com/uhn/ggql/pkg/ggql"
)

func TestFragment(t *testing.T) {
	f := ggql.Fragment{
		Name: "friend",
		Inline: ggql.Inline{
			Condition: &ggql.Ref{Base: ggql.Base{N: "User"}},
			SelBase: ggql.SelBase{
				Sels: []ggql.Selection{
					&ggql.Field{Name: "name"},
					&ggql.Field{
						Name: "picture",
						Args: []*ggql.ArgValue{{Arg: "size", Value: ggql.Var("size")}},
					},
				},
			},
		},
	}
	checkEqual(t, `fragment friend on User {
  name
  picture(size: $size)
}
`, f.String(), "Fragment.String() mismatch")
}
